package com.ibrahim.sample.db;

public class Artist {
    public long id;
    public String name;
    public String company;
    public int age;

    public Artist(long id, String name, String company, int age) {
        this.id = id;
        this.name = name;
        this.company = company;
        this.age = age;
    }
}
