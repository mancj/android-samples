package com.ibrahim.sample.db;

import java.util.ArrayList;
import java.util.List;

public class FakeArtistDatabase {

    private static final List<Artist> artists = new ArrayList<>();

    static {
        artists.add(new Artist(1, "Artist 1", "Company 1", 20));
        artists.add(new Artist(2, "Artist 2", "Company 2", 21));
        artists.add(new Artist(3, "Artist 3", "Company 3", 22));
        artists.add(new Artist(4, "Artist 4", "Company 4", 23));
        artists.add(new Artist(5, "Artist 5", "Company 5", 24));
        artists.add(new Artist(6, "Artist 6", "Company 6", 25));
        artists.add(new Artist(7, "Artist 7", "Company 7", 26));
        artists.add(new Artist(8, "Artist 8", "Company 8", 27));
    }

    public static List<Artist> getArtists() {
        return artists;
    }

    public static Artist getArtist(long id) {
        for (Artist artist : artists) {
            if (artist.id == id) {
                return artist;
            }
        }
        return null;
    }

}
