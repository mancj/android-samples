package com.ibrahim.sample.artists;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ibrahim.sample.R;
import com.ibrahim.sample.adapter.ArtistsAdapter;
import com.ibrahim.sample.adapter.OnArtistClickListener;
import com.ibrahim.sample.db.Artist;
import com.ibrahim.sample.db.FakeArtistDatabase;

import java.util.List;

public class ArtistsListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        List<Artist> artists = FakeArtistDatabase.getArtists();

        ArtistsAdapter adapter = new ArtistsAdapter();
        adapter.setArtists(artists);
        adapter.setOnArtistClickListener(new OnArtistClickListener() {
            @Override
            public void onArtistClickListener(Artist artist) {
                Intent intent = new Intent(ArtistsListActivity.this, ArtistDetailActivity.class);
                intent.putExtra(ArtistDetailActivity.EXTRA_ARTIST_ID, artist.id);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
