package com.ibrahim.sample.artists;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ibrahim.sample.R;
import com.ibrahim.sample.db.Artist;
import com.ibrahim.sample.db.FakeArtistDatabase;

public class ArtistDetailActivity extends AppCompatActivity {
    public static final String EXTRA_ARTIST_ID = "artist_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_detail);
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        long artistId = getIntent().getLongExtra(EXTRA_ARTIST_ID, -1);

        Artist artist = FakeArtistDatabase.getArtist(artistId);

        TextView nameView = findViewById(R.id.nameView);
        TextView companyView = findViewById(R.id.companyView);
        TextView ageView = findViewById(R.id.ageView);

        nameView.setText(artist.name);
        companyView.setText(artist.company);
        ageView.setText(String.valueOf(artist.age));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
