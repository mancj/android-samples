package com.ibrahim.sample.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ibrahim.sample.R;
import com.ibrahim.sample.db.Artist;

import java.util.List;

public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ArtistHolder> {
    private List<Artist> artists;
    private OnArtistClickListener onArtistClickListener;

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
        notifyDataSetChanged();
    }

    public void setOnArtistClickListener(OnArtistClickListener onArtistClickListener) {
        this.onArtistClickListener = onArtistClickListener;
    }

    @NonNull
    @Override
    public ArtistHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_artist_list, parent, false);
        return new ArtistHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistHolder holder, int position) {
        final Artist artist = artists.get(position);
        holder.bind(artist);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onArtistClickListener != null) {
                    onArtistClickListener.onArtistClickListener(artist);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    static class ArtistHolder extends RecyclerView.ViewHolder {

        ArtistHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(Artist artist) {
            TextView nameView = itemView.findViewById(R.id.nameView);
            TextView companyView = itemView.findViewById(R.id.companyView);
            TextView ageView = itemView.findViewById(R.id.ageView);

            nameView.setText(artist.name);
            companyView.setText(artist.company);
            ageView.setText(String.valueOf(artist.age));
        }

    }

}
