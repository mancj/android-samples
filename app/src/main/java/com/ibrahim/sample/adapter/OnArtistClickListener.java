package com.ibrahim.sample.adapter;

import com.ibrahim.sample.db.Artist;

public interface OnArtistClickListener {
    void onArtistClickListener(Artist artist);
}
